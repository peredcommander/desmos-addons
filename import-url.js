{
function alertBox(title, elements, onSubmit){
  let alert = document.createElement("div");
  alert.style.cssText += `
  	width: 300px;
		height: 300px;
		left: 50%;
		top: 50%;
		border: 5px solid black;
		transform: translateX(-50%) translateY(-50%);
		position: absolute;
    display: flex;
	  justify-content: center;
  	align-items: center;
    background: white;
    z-index: 1000;
    flex-direction:column;`;
  let titleElem = document.createElement("h1");
  titleElem.innerText = title;
  titleElem.style.cssText += `
  	top: 0px;
    position: absolute;`;
  alert.appendChild(titleElem);
	elements.forEach(elem=>alert.appendChild(elem));
  let closeButton = document.createElement("button");
  closeButton.innerText = "Done";
  closeButton.style.cssText = `
  	bottom: 10px;
    position: absolute;
    display: none`;
  alert.appendChild(closeButton);
  document.body.appendChild(alert);
  closeButton.onclick = ()=>{alert.remove(); onSubmit()};
  let obj = {
  	show(){closeButton.style.display='';return this.hide},
    hide(){closeButton.style.display='none';return this.show}
  };
  obj.show = obj.show.bind(obj);
  obj.hide = obj.hide.bind(obj);
  return obj.show;
}
function submit(){
	fetch(input.value).then(response=>response.text()).then(load);
}
function load(text){
	let addon = JSON.parse(text);
  let folders = Calc.getState().expressions.list.filter(exp=>exp.type=="folder").map(exp=>exp.title);
  let dependencies = addon.dependencies;
  let unresolved = dependencies.filter(depend=>!folders.includes(depend));
  if (unresolved.length){
  	alert("Unresolved dependencies: " + unresolved.join(', '));
    throw "Unresolved dependencies: " + unresolved.join(', ');
  }
  let keybinds = addon.keybinds;
  keybinds = [...Object.entries(keybinds)].map(keyVal=>{
  	let key = keyVal[0].split('');
    let obj = {};
    obj.val = keyVal[1];
    obj.key = key.pop().charCodeAt(0);
    obj.ctrl = key.includes('c');
    obj.alt = key.includes('a');
    return obj;
  });
  let expression = Calc.getExpressions()[1];
  let cNota = (expression.latex.match(/(?<=c-a=1:\\left\\{).*?(?=\\right\\})/)||[""])[0].split(',');
  let aNotc = (expression.latex.match(/(?<=a-c=1:\\left\\{).*?(?=\\right\\})/)||[""])[0].split(',');
  let cAnda = (expression.latex.match(/(?<=ac=1:\\left\\{).*?(?=\\right\\})/)||[""])[0].split(',');
  keybinds.forEach(obj=>{
  	if (obj.ctrl){
    	if (obj.alt){
      	return cAnda.push(`k=${obj.key}:${obj.val}`);
      }
      return cNota.push(`k=${obj.key}:${obj.val}`);
    }
    return aNotc.push(`k=${obj.key}:${obj.val}`)
  });
  cNota = cNota.join(',');
  aNotc = aNotc.join(',');
  cAnda = cAnda.join(',');
  let newText = "s_{hortCut}\\left(k,c,a\\right)=\\left\\{";
  if (cNota) newText += `c-a=1:\\left\\{${cNota}\\right\\}`;
  if (cAnda) newText += `ca=1:\\left\\{${cAnda}\\right\\}`;
  if (aNotc) newText += `a-c=1:\\left\\{${aNotc}\\right\\}`;
  newText += "\\right\\}";
  Calc.setExpression({id:expression.id, latex:newText});
  let graph = addon.graph;
  fetch(graph, {"headers":{"Accept":"application/json"}}).then(response=>response.json()).then(importGraph);
  /*if I was adasba I wouldn't have this commented out
  fetch("https://inparencomma.pythonanywhere.com/reportEmail?email=" + Calc.user.getEmail());
  */
}
function importGraph(data){
	let state = Calc.getState();
	let old = state.expressions.list;
  let other = data.state.expressions.list;
  state.expressions.list = old.concat(other);
  Calc.setState(state);
}
let input = document.createElement("input");
let done = alertBox("Addon Import", [document.createTextNode("input a url to the json file of an addon"),input], submit);
let undo = done();
undo();
input.onchange = ()=>{if(input.value)done();else undo()}
}